import type { NextPage } from 'next';
import dynamic from 'next/dynamic';
import { useGetDataApi } from '../src/hook/useGetData';
import { DetailMovieTypes } from '../src/services/model/data.model';

const CardContent = dynamic(() => import('../src/components/Card/CardContent'));

const Home: NextPage = () => {
  const { movies } = useGetDataApi('popular');
  return (
    <section>
      <h1 className="text-2xl mt-2 p-4 font-bold ">Simple Movie App</h1>
      <div
        style={{ margin: '0 auto' }}
        className="grid auto-rows-auto sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4  w-full"
      >
        {movies?.map((data: DetailMovieTypes) => (
          <CardContent
            key={data.id}
            title={data.original_title}
            description={data.overview}
            imageURl={data.poster_path}
            id={data.id}
            rate={data.vote_average}
          />
        ))}
      </div>
    </section>
  );
};

export default Home;
