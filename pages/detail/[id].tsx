import Image from 'next/image';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import Button from '../../src/components/Button/Button';
import { img_path, NoImageUrl } from '../../src/components/Card/CardContent';
import Rating from '../../src/components/Rating/Rating';
import { useGetDataApi } from '../../src/hook/useGetData';
import { DetailMovieTypes } from '../../src/services/model/data.model';
import { LeftOutlined } from '@ant-design/icons';
import Link from 'next/link';
import { format } from 'date-fns';

const detailData = (id: any, data: Array<DetailMovieTypes>): any => {
  return data.filter((data: DetailMovieTypes) => data.id == id)[0];
};

const DetailPage = () => {
  const [detail, setDetail] = useState<DetailMovieTypes>();
  const [srcImgBig, setSrcImgBig] = useState<any>();
  const [srcImg, setSrcImg] = useState<any>();
  const { movies } = useGetDataApi('popular');
  const router = useRouter();
  const { id } = router.query;

  useEffect(() => {
    if (typeof window !== 'undefined') {
      if (movies.length > 1) {
        setDetail(detailData(id, movies));
      }
    }
  }, [id, movies]);

  useEffect(() => {
    if (detail) {
      setSrcImg(img_path + '/w500' + detail.poster_path);
      setSrcImgBig(img_path + '/w500' + detail.backdrop_path);
    }
  }, [detail]);

  return (
    detail && (
      <section className="my-6 md:pb-32">
        <div className="absolute h-2/6 md:h-4/6 w-full top-0 right-0">
          <div className="relative w-full h-full">
            <Image
              alt="example"
              style={{ border: 0, outline: 0 }}
              layout="fill"
              className="w-full h-full"
              onError={() => setSrcImgBig(NoImageUrl)}
              src={srcImgBig}
            />
            <div className="section-backdrop"></div>
          </div>
        </div>
        <div className="flex mt-64 md:mt-[30rem]">
          <div className="relative  w-28 h-36 md:w-80 md:min-w-[300px] md:h-96 md:relative">
            <Image
              alt="example"
              layout="fill"
              width={200}
              // height={100}
              onError={() => setSrcImg(NoImageUrl)}
              src={srcImg}
            />
          </div>
          <div className="ml-4 ">
            <h1 className="font-bold text-sm md:text-3xl ">{detail?.title}</h1>
            <p className="md:text-lg md:font-base text-sm mt-4">
              Release Date : {format(new Date(detail?.release_date), 'PPP')}
            </p>
            <p className="md:text-lg md:font-base text-sm">
              Popularity : {detail?.popularity}
            </p>
            <Rating rate={detail?.vote_average} />
            <p className="mt-2 text-justify md:block hidden text-sm  md:text-xl">
              {detail?.overview}
            </p>
          </div>
        </div>

        <p className="mt-2 text-justify md:hidden">{detail?.overview}</p>
        <Link href="/">
          <div className="my-6 float-left cursor-pointer">
            <Button name="Back" icon={<LeftOutlined />} />
          </div>
        </Link>
      </section>
    )
  );
};

export default DetailPage;
