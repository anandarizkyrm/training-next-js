import { useEffect, useState } from 'react';
import { DetailMovieTypes } from '../services/model/data.model';
import { getMovies } from '../services/service/api.service';
import { setToLocalStorage } from '../utils';

export const useGetDataApi = (param: string) => {
  const [movies, setMovies] = useState<Array<DetailMovieTypes>>([]);
  const [error, setError] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response: any = await getMovies(param);
        setToLocalStorage('movies', response.results);

        setMovies(response.results);
      } catch (error: any) {
        setError(error);
      }
      if (typeof window !== 'undefined') {
        if (!window.navigator.onLine && localStorage.movies) {
          setMovies(JSON.parse(localStorage.movies));
        }
      }
    };
    fetchData();
  }, []);

  return {
    movies,
    error,
    setMovies,
  };
};
