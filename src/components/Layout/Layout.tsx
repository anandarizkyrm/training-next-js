import { ArrowUpOutlined } from '@ant-design/icons';
import { BackTop } from 'antd';
import ButtonTheme from '../Button/ButtonTheme';

function Layout({ children }: any) {
  return (
    <div className="px-4 md:px-12 md:py-4  mt-2">
      {children}
      <ButtonTheme />
      <BackTop className="right-6  bottom-20  fixed">
        <div className="back_top rounded-full flex justify-center items-center">
          <ArrowUpOutlined />
        </div>
      </BackTop>
    </div>
  );
}

export default Layout;
