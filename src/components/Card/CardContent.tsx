import { Button, Card } from 'antd';
import Image from 'next/image';
import Link from 'next/link';
import { useState } from 'react';
import { sliceStr } from '../../utils';
import Rating from '../Rating/Rating';

export const NoImageUrl = '/noImg.jpg';
export const img_path = process.env.NEXT_PUBLIC_IMG;

const CardContent = ({
  imageURl,
  title,
  description,
  id,
  rate,
}: {
  imageURl?: string | undefined;
  title: string;
  description: string;
  id: string;
  rate: number;
}) => {
  const [srcImg, setSrcImg] = useState(
    imageURl ? img_path + '/w500' + imageURl : NoImageUrl
  );
  return (
    <div className="w-full py-2 px-4">
      <Card
        hoverable
        style={{ width: '100%', maxWidth: '22rem' }}
        className="card"
        bodyStyle={{ padding: '12px' }}
        cover={
          <div className="h-full w-full overflow-hidden relative">
            <Image
              alt="img"
              width={353}
              height={540}
              onError={() => setSrcImg(NoImageUrl)}
              src={srcImg}
            />
          </div>
        }
      >
        <h1 className="font-semibold text-xl">{title}</h1>
        <p className="p-card text-justify">{sliceStr(description, 100)}</p>

        <Rating rate={rate} />
        <Link href={`/detail/${id}`}>
          <Button
            style={{
              float: 'right',
              background: 'red',
              color: 'white',
              marginTop: '6px',
            }}
            className="bg-black"
            type="link"
          >
            View More
          </Button>
        </Link>
      </Card>
    </div>
  );
};

export default CardContent;
