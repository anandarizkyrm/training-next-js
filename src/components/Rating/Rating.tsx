import { Rate } from 'antd';

function Rating({ rate }: { rate: number | undefined | any }) {
  return (
    <div className="flex justify-start items-center mt-2">
      <div>
        <Rate allowHalf disabled defaultValue={rate / 2} count={5} />
      </div>
      <p className="ml-2 text-left font-bold">{rate / 2}</p>
    </div>
  );
}

export default Rating;
