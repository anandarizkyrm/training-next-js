export const sliceStr = (letter: string, max: number) => {
  if (letter.length > max) {
    return letter.slice(0, max) + '...';
  }
  return letter;
};

export const setToLocalStorage = (key: string, value: any) => {
  localStorage.setItem(key, JSON.stringify(value));
};
