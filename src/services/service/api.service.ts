const rootAPI = process.env.NEXT_PUBLIC_API;
const keyAPI = process.env.NEXT_PUBLIC_KEY;

export async function getMovies(param: string, page = 1) {
  const response = await fetch(
    `${rootAPI}/movie/${param}?${keyAPI}&page=${page}`
  );
  const data = await response.json();

  return data;
}
