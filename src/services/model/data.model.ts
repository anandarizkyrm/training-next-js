export interface CategoryTypes {
  id: number;
  name: string;
}

export interface DetailMovieTypes {
  id: string;
  backdrop_path: string;
  poster_path: string;
  title: string;
  release_date: string;
  vote_average: number;
  vote_count: number;
  original_title: string;
  runtime: number;
  status: string;
  overview: string;
  original_language: string;
  popularity: number;
  budget: number;
  revenue: number;
  genres: CategoryTypes[];
}
